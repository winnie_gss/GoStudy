package main

import (
	"reflect"
	"fmt"
)

type MessageBox struct {
	Name    string `json:"JName" tag1:"TagName"`  //tag是结构体的原信息, 可以在运行时通过反射的机制读取出来
	//title   string //小写私有, 别的包是无法访问的
	Age     int    `json:"JAge" tag1:"TagAge"`
	Content string `json:"JContent" tag1:"TagContent"`
}

func (box MessageBox) Msg(name string, content string) {
	fmt.Printf("Hello %s, my name is %s \r\n", box.Name, name)
	fmt.Println(content)
}

func exam1(i interface{}) {
	reType := reflect.TypeOf(i)
	reValue := reflect.ValueOf(i)

	kind := reType.Kind()

	switch kind {
	case reflect.Int:
		fmt.Println("Int:", reValue.Int())
	case reflect.String:
		fmt.Println("String:", reValue.String())
	case reflect.Ptr:
		fmt.Println("Ptr", reValue.Elem().Type(), reValue.Elem().Interface())
		reValue.Elem().SetString("Hello World")
	default:
		fmt.Println("Other")
	}
}

func exam2(i interface{}) {
	reType := reflect.TypeOf(i)
	reValue := reflect.ValueOf(i)
	kind := reType.Kind()

	if kind == reflect.Struct {
		NumField := reType.NumField()
		for i := 0; i < NumField; i++ {
			fmt.Println(reType.Field(i).Type, reValue.Field(i).Interface(), reType.Field(i).Tag.Get("tag1"))
		}
	}
}

func exam3(i interface{}) {
	reValue := reflect.ValueOf(i)

	reMethod := reValue.MethodByName("Msg")
	args := []reflect.Value{reflect.ValueOf("cName"), reflect.ValueOf("This is a Content")}
	reMethod.Call(args)
}

func main() {
	//TODO : 判断传入的类型
	/*
	str := "Hello"
	exam1(&str)
	fmt.Println(str)
	*/

	//TODO : 获取结构信息
	exam2(MessageBox{Name: "张三", Age: 20, Content: "Hello World"})

	//TODO : 通过发射进行方法的调用 动态调用方法
	//exam3(MessageBox{Name: "SOURCE-OPEN", Age: 20, Content: "Hello World"})
}

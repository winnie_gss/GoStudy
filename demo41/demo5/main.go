package main

import "fmt"

func Add1(number [5]int) {
	number[0] = 99
}

func Add2(number []int) {
	number[0] = 99
}

func main() {
	var number1 [5]int = [5]int{1, 2, 3, 4, 5}
	var number2 []int = []int{1, 2, 3, 4, 5}

	Add1(number1)
	Add2(number2)

	fmt.Println(number1)
	fmt.Println(number2)
}

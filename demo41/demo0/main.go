package main

//官方建议写法(给返回值定义一个名称)
func Add(num1, num2 int) (num3 int) {
	num3 = num1 + num2
	return
}

func main() {
	/*
	//定义数组时, 指定数组元素个数必须是常量
	//non-constant array bound a
	a := 10
	var b [a]int
	fmt.Println(b)
	*/
}

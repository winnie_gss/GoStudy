package main

import (
	"fmt"
	"time"
)

func main() {

	ch := make(chan int, 2)

	go func(ch chan int) {
		ch <- 1
		ch <- 2
		ch <- 3
		ch <- 4
		ch <- 5
		ch <- 6
		ch <- 7
	}(ch)

	go func(ch chan int) {
		for{
			select {
			case c := <-ch:
				fmt.Println(c)
			}
		}
	}(ch)

	time.Sleep(time.Second * 60)
}

package main

import "fmt"

func main() {

	a := 10
	b := &a

	//操作符"&"取变量地址,使用"*"通过指针间接访问目标对象
	//&	取地址运算符	&a	变量a的地址
	//*	取值运算符	*a	指针变量a所指向内存的值
	fmt.Println(&a)
	fmt.Println(b)
	fmt.Println(*b) // * 取地址的值
	fmt.Println(&b) // & 取值

	var c *int

	//*c = 666 	//err, c没有合法指向(nil 没有指向, 没有空间)

	//Go的指针不支持运算

	c = new(int) //new 分配内存空间
	*c = 666

	fmt.Println(*c)
}

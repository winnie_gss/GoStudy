package main

import "fmt"

// Go 面向对象语言 ? Yes and No
// http://kangkona.github.io/oo-in-golang/


type Person struct {
	Name string
	Age  int
}

func main() {

	p1 := new(Person)
	fmt.Println(&p1)
	fmt.Println(&p1.Name)
	fmt.Println(&p1.Age)

	//继承   单继承 ,  Go是多继承
	//封装   结构字段属性的大小写
	//多态  定义一个结构, 用结构去实现接口的方法  .. return 一个接口时, 可以return 实现接口的结构

}
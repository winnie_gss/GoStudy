package main

import "fmt"

func main() {

	//生命周期
	//defer 函数结束前调用
	func (){
		defer fmt.Println(1)
	}()

	func(){
		defer fmt.Println(2)
	}()

	defer fmt.Println(3)
	defer fmt.Println(4)

}

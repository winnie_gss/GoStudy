package main

import (
	"fmt"
	"net/http"
)

func main() {

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {

		/*致命错误, 只退出当前协程
		var i interface{} = 1
		x := i.(string)
		*/

		fmt.Println(request)
		fmt.Println("--------------------------------")

	})

	http.ListenAndServe(":9001", nil)

}

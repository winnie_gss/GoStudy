package main

import "fmt"

func exam1() {
	var number1 []string
	number2 := []string{"A", "B", "C"}

	number3 := make([]int, 2, 2)
	number4 := append(number3, []int{3, 4, 5}...) //number4 := append(number3,number3...)//用省略号自动展开切片,以使用每个元素

	//长度和容量 切片再切片
	//cap 省略时 默认等于 len
	//number5... 展开number5切片成一个个元素
	// var a[5]int = [5]int{5, 4, 3, 2, 1} //sort.Ints(a[:])

	fmt.Println("Number1:", number1)
	fmt.Println("Number2:", number2)
	fmt.Printf("Number3: %d, len:%d, cap:%d\n", number3, len(number3), cap(number3))
	fmt.Printf("Number4: %d, len:%d, cap:%d\n", number4, len(number4), cap(number4))

	number5 := []string{"A", "B", "C"}
	number6 := []string{"D", "E"}
	number7 := copy(number6, number5) //等位替换原切片(返回替换成功的个数)
	fmt.Printf("Number6: copy success number %d --- %s\n", number7, number6)
}

func exam2() {
	//[start:end] [start:] [:end] [:]
	number8 := []int{9, 12, 35, 41, 95, 60, 17, 66, 9, 10}
	fmt.Println("Number8-1:", number8[3:6])

	//切片是引用类型(对数组的引用)
	fmt.Printf("Number8-2: %d, Addr:%p, Len:%d, Cap:%d \r\n", number8, number8, len(number8), cap(number8))

	number9 := number8
	number9[3] = 77
	fmt.Printf("Number8-3: %d, Addr:%p, Len:%d, Cap:%d", number8, number8, len(number8), cap(number8))

	fmt.Println("")
	number8 = append(number8, 11)
	number9[3] = 88
	fmt.Printf("Number8-4: %d, Addr:%p Len:%d, Cap:%d", number8, number8, len(number8), cap(number8))
}

//slice不是用来当做链表或者栈用, container/list

// make([]T, length长度, capacity容量)
// 一般使用make()创建 : 用len获取切片长度, cap获取切片容量
// 一个切片在未初始化之前默认为 nil，长度为 0
// 切片是基于数组类型做的一层封装,自动扩容
// 与数组相比切片的长度是不固定的,可以追加元素,在追加时可能使切片的容量增大
func main() {
	exam1()
}

package main

import (
	"net/http"
	"fmt"
	"html/template"
	"strconv"
	"time"
)

//TODO : 首页
func Index(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	sort := r.FormValue("sort")
	sort2, err := strconv.Atoi(sort)

	content := make(map[string]interface{}, 2)
	content["sort"] = sort2
	content["data"], err = SelectAll(sort2)
	if err != nil {
		fmt.Println(err)
		return
	}

	tpl, err := template.ParseFiles("view/index.html")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Println(err)
	}
}

//TODO : 创建
func Create(w http.ResponseWriter, r *http.Request) {
	tpl, err := template.ParseFiles("view/create.html")
	if err != nil {
		fmt.Println(err)
	}

	err = tpl.Execute(w, nil)
	if err != nil{
		fmt.Println(err)
	}
}

func CreateForm(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	title := r.FormValue("title")
	content := r.FormValue("content")

	id := InsertOne(title, content)

	if id > 0 {
		http.Redirect(w, r, "/index", 302)
	}
}

//TODO : 编辑
func Editor(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Println(err)
		return
	}

	data := SelectOne(id)

	tpl, err := template.ParseFiles("view/editor.html")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = tpl.Execute(w, data)
	if err != nil {
		fmt.Println(err)
	}
}

func EditorForm(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Println(err)
	}
	title := r.FormValue("title")
	content := r.FormValue("content")

	id2 := UpdateOne(id, title, content)
	if id2 > 0 {
		http.Redirect(w, r, "/index", 302)
	}
}

//TODO : 删除
func Delete(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Println(err)
	}

	id2 := DeleteOne(id)
	if id2 > 0 {
		http.Redirect(w, r, "/index", 302)
	}
}

//TODO : 详情
func View(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		fmt.Println(id)
	}

	content := ViewOne(id)
	content.Time2 = time.Unix(content.Time, 0).Format("2006-01-02 15:04:05")

	tpl, err := template.ParseFiles("view/view.html")
	if err != nil {
		fmt.Println(err)
	}
	tpl.Execute(w, content)
}

func main() {

	initDb()

	//fsh := http.FileServer(http.Dir("E:/goStudy/src/gitee.com/winnie_gss/GoStudy/_demo2/static"))
	fsh := http.FileServer(http.Dir("D:/GoStudy/src/gitee.com/winnie_gss/GoStudy/_demo2/static"))
	http.Handle("/static/", http.StripPrefix("/static/", fsh))

	http.HandleFunc("/index", Index)
	http.HandleFunc("/create", Create)
	http.HandleFunc("/createform", CreateForm)
	http.HandleFunc("/editor", Editor)
	http.HandleFunc("/editorform", EditorForm)
	http.HandleFunc("/delete", Delete)
	http.HandleFunc("/view", View)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/index", 302)
	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
	}

}

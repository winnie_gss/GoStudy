package main

import (
	"fmt"
	"time"
	"github.com/jmoiron/sqlx"
	_ "github.com/go-sql-driver/mysql"
)

var DB *sqlx.DB

func initDb() {
	var err error

	dbName := "messagebox"
	dbUser := "root"
	dbPassword := "123456"
	dbPort := 3306

	dsn := fmt.Sprintf("%s:%s@tcp(localhost:%d)/%s", dbUser, dbPassword, dbPort, dbName)
	DB, err = sqlx.Open("mysql", dsn)

	if err != nil {
		fmt.Println(err)
		return
	}
}

type Article struct {
	Id      int    `db:"id"`
	Title   string `db:"title"`
	Content string `db:"content"`
	ViewNum string `db:"viewnum"`
	Time    int64  `db:"time"`
	Time2   string
}

func SelectOne(id int) Article {

	var article Article

	sql := "select id, title, content from msg_article where status = 'A' and id = ?"

	err := DB.Get(&article, sql, id)
	if err != nil {
		fmt.Println(err)
	}
	return article
}

func ViewOne(id int) Article {

	sql2 := "update msg_article set viewnum=viewnum+1 where status = 'A' and id = ?"
	_, err := DB.Exec(sql2, id)
	if err != nil {
		fmt.Println(err)
	}

	var article Article

	sql := "select id, title, content, viewnum, time from msg_article where status = 'A' and id = ?"

	err = DB.Get(&article, sql, id)
	if err != nil {
		fmt.Println(err)
	}
	return article
}

func SelectAll(sort int) ([]Article, error) {
	var article []Article

	flag := "time"
	if sort == 1 {
		flag = "viewnum"
	}

	sql := fmt.Sprintf("select id, title, content, viewnum, time from msg_article where status='A' order by %s desc limit 10", flag)
	err := DB.Select(&article, sql)

	for k, v := range article {
		article[k].Time2 = time.Unix(v.Time, 0).Format("2006-01-02 15:04:05")
	}

	return article, err
}

func InsertOne(title, content string) (id int64) {
	timeUx := time.Now().Unix()
	stmt, err := DB.Prepare("insert into msg_article(`title`, `content`, `time`) values(?, ?, ?)")

	if err != nil {
		fmt.Println(err)
		return
	}

	result, err := stmt.Exec(title, content, timeUx)
	if err != nil {
		fmt.Println(err)
		return
	}

	id, err = result.LastInsertId()
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

func UpdateOne(id int, title, content string) (id2 int64) {
	timeUx := time.Now().Unix()
	stmt, err := DB.Prepare("update msg_article set title=?, content=?, time=? where id = ? and status = 'A'")

	if err != nil {
		fmt.Println(err)
		return
	}

	result, err := stmt.Exec(title, content, timeUx, id)
	if err != nil {
		fmt.Println(err)
		return
	}

	id2, err = result.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

func DeleteOne(id int) (id2 int64) {
	stmt, err := DB.Prepare("update msg_article set status = 'D' where id = ? and status = 'A'")

	if err != nil {
		fmt.Println(err)
		return
	}

	result, err := stmt.Exec(id)
	if err != nil {
		fmt.Println(err)
		return
	}

	id2, err = result.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

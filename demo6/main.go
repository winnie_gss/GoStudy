package main

import "fmt"

type Human interface {
	Content()
}

type Teacher struct {
	Name           string
	TeacherSubject string
}

type Student struct {
	Name  string
	Score float32
}

func (t *Teacher) Content() {
	fmt.Printf("Teacher: my name is %s, teachering %s \r\n", t.Name, t.TeacherSubject)
}

func (s *Student) Content() {
	fmt.Printf("Student: my name is %s, score %.1f \r\n", s.Name, s.Score)
}

func SayContent(h Human) {
	h.Content()
}

func main() {
	fmt.Println("Begin Say")

	//new(Teacher) 和&Teacher{} 本质上是一样的,都是返回结构体地址

	t := new(Teacher)
	t = &Teacher{Name: "张三", TeacherSubject: "计算机"}
	SayContent(t)

	s := &Student{Name: "李四", Score: 80.5}
	SayContent(s)
}

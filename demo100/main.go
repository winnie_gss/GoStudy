package main

import (
	"net"
	"fmt"
	"time"
)

func main() {
	linstener, err := net.Listen("tcp", "127.0.0.1:9005")
	if err != nil {
		fmt.Println(err)
	}

	for {
		conn, err := linstener.Accept()
		if err != nil {
			fmt.Println(err)
		}

		time.Sleep(time.Second * 1)
		conn.Write([]byte("HTTP/1.1 200 OK\r\nContent-Length: 10\r\nContent-Type: text/plain; charset=utf-8\r\n\r\nHelloWorld"))
	}

}

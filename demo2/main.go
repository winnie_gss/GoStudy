package main

import "fmt"

//如果在数组的长度位置出现的是“...”省略号，则表示数组的长度是根据初始化值的个数来计算
//数组的长度必须是常量表达式，因为数组的长度需要在编译阶段确定
//数组后面无法追加元素

//定义和传递数组是值拷贝, 为了避免复制可以定义一个指针指向数组
func main() {
	var number1 [5]int
	number2 := [5]int{1, 2, 3, 4, 5}
	//number3 := [1]string{8: "a"} //Index out of bounds
	number4 := [...]string{8: "a", 6: "c"}
	number5 := [2][3][4]int{//三维数组
		{
			{1, 1, 1, 1}, {3, 3, 3, 3}, {5, 5, 5, 5},
		},
		{
			{2, 2, 2, 2}, {4, 4, 4, 4}, {6, 6, 6, 6},
		},
	}
	number5[0][1][2] = 12

	fmt.Println("Number1:", number1)
	fmt.Println("Number2:", number2)
	fmt.Printf("Number4: %s---len:%d\n", number4, len(number4))
	fmt.Println("Number5:", number5)

	number1[3] = 5
	fmt.Println("Number1:", number1)

	//数组是值类型
	number6 := number2
	number6[1] = 6
	fmt.Printf("Number2: %d---Number6:%d", number2, number6)
}

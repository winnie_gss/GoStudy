package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"strconv"
	"time"
)

//TODO : 区块
type Block struct {
	Timestamp     int64
	PrevBlockHash []byte
	Hash          []byte
	Data          []byte
}

//TODO : 区块链
type BlockChain struct {
	blocks []*Block
}

//TODO : 新增区块
func NewBlock(data string, prevBlockHash []byte) *Block {
	block := &Block{
		Timestamp:     time.Now().Unix(),
		PrevBlockHash: prevBlockHash,
		Data:          []byte(data),
	}
	block.SetHash()
	return block
}

//TODO : 计算Hash
// Hash = sha256(PrevBlockHash + Data + Timestamp)
func (b *Block) SetHash() {
	timestamp := []byte(strconv.FormatInt(time.Now().Unix(), 10))
	hash := sha256.Sum256(bytes.Join([][]byte{b.PrevBlockHash, b.Data, timestamp}, []byte{}))
	b.Hash = hash[:]
}

//TODO : 将新增的块添加到链上
func (blockchain *BlockChain) AddBlock(data string) {
	prevBlockHash := blockchain.blocks[len(blockchain.blocks)-1].Hash
	block := NewBlock(data, prevBlockHash)
	blockchain.blocks = append(blockchain.blocks, block)
}

//TODO : 创建初始链
func NewBlockChain() *BlockChain {
	return &BlockChain{[]*Block{NewBlock("Genesis Block", []byte{})}}
}

func main() {
	blockchain := NewBlockChain()

	blockchain.AddBlock("MessageBox1")
	blockchain.AddBlock("MessageBox2")

	for _, v := range blockchain.blocks {
		fmt.Printf("PrevBlockHash: %x\n", v.PrevBlockHash)
		fmt.Printf("Hash: %x\n", v.Hash)
		fmt.Printf("Data: %s\n\n", v.Data)
	}
}

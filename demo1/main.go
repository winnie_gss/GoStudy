//当前程序的包名(一个可执行程序只有一个 main 包)
//一般建议package的名称和目录名保持一致
package main

// 导入其它包
// 缺少或未使用的包,程序都无法编译通过
// _ 需要这个包的实例化,没用到其它函数
import (
	"fmt"
	//_ "github.com/go-sql-driver/mysql"
)

//只有 package 名称为 main 的包才可以包含 main 函数
func main() {
	fmt.Println("Hello World")
}

//go build -o bin\demo1.exe demo1\main.go
//go install
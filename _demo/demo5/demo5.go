package demo5

import (
	"fmt"
	"sort"
	"time"
	"math/rand"
)

func TryString() {
	//如果在数组的长度位置出现的是“...”省略号，则表示数组的长度是根据初始化值的个数来计算
	//数组的长度必须是常量表达式，因为数组的长度需要在编译阶段确定
	//数组后面无法追加元素

	var number1 [5]int
	number2 := [5]int{1, 2, 3, 4, 5}
	//number3 := [1]string{8: "a"} //Index out of bounds
	number4 := [...]string{8: "a", 6: "c"}
	number5 := [2][3][4]int{//三维数组
		{
			{1, 1, 1, 1}, {3, 3, 3, 3}, {5, 5, 5, 5},
		},
		{
			{2, 2, 2, 2}, {4, 4, 4, 4}, {6, 6, 6, 6},
		},
	}
	number5[0][1][2] = 12

	fmt.Println("Number1", number1)
	fmt.Println("Number2", number2)
	fmt.Printf("Number4:%s---len:%d\n", number4, len(number4))
	fmt.Println("Number5", number5)

	number1[3] = 5
	fmt.Println("Number1", number1)

	//数组是值类型
	number6 := number2
	number6[1] = 6
	fmt.Printf("Number2:%d---Number6:%d", number2, number6)
}

func TrySlice() {
	// make([]T, length长度, capacity容量)
	// 一般使用make()创建 : 用len获取切片长度, cap获取切片容量
	// 一个切片在未初始化之前默认为 nil，长度为 0
	//切片是基于数组类型做的一层封装,自动扩容
	// 与数组相比切片的长度是不固定的,可以追加元素,在追加时可能使切片的容量增大

	var number1 []string
	number2 := []string{"A", "B", "C"}

	number3 := make([]int, 2, 2)
	number4 := append(number3, 1) //number4 := append(number3,number3...)//用省略号自动展开切片,以使用每个元素

	fmt.Println(number1)
	fmt.Println(number2)
	fmt.Printf("number3:%d, len:%d, cap:%d\n", number3, len(number3), cap(number3))
	fmt.Printf("number4:%d, len:%d, cap:%d\n", number4, len(number4), cap(number4))

	number5 := []string{"A", "B", "C"}
	number6 := []string{"D", "E"}
	number7 := copy(number6, number5) //等位替换原切片
	fmt.Printf("copy success number%d --- %s\n", number7, number6)

	//[start:end] [start:] [:end] [:]
	number8 := []int{9, 12, 35, 41, 95, 60, 17, 66, 9, 10}
	fmt.Println(number8[3:6])

	//切片是引用类型(对数组的引用)
	fmt.Printf("Number8:%d, Addr:%p, Len:%d, Cap:%d", number8, number8, len(number8), cap(number8))
	fmt.Println("")
	number9 := number8
	number9[3] = 77
	fmt.Printf("Number8:%d, Addr:%p, Len:%d, Cap:%d", number8, number8, len(number8), cap(number8))

	fmt.Println("")
	number8 = append(number8, 11)
	number9[3] = 88
	fmt.Printf("Number8:%d, Addr:%p Len:%d, Cap:%d", number8, number8, len(number8), cap(number8))

	//长度和容量 切片再切片
	//number5... 展开number5切片成一个个元素
	// var a[5]int = [5]int{5, 4, 3, 2, 1} //sort.Ints(a[:])

}

func TryMap() {

	//map的key必须是支持==或!=比较运算的类型,不可以是函数,map或slice
	//map的value 则没有任何的限制
	//make 根据size 大小来初始化分配内存，不过分配后的 map 长度为0。 如果 size 被忽略了，那么会在初始化分配内存的时候 分配一个小尺寸的内存
	//map的迭代顺序是随机的

	var number1 map[int]string
	number2 := map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5}
	number3 := make(map[int]string, 5)

	fmt.Println(number1)
	fmt.Println(number2)
	fmt.Println(number3, len(number3))

	delete(number2, "c")
	fmt.Println(number2)

	number2["f"] = 6
	fmt.Println(number2)

	//引用类型
	var number4 = map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 4}
	fmt.Println("Number4:", number4)
	number5 := number4
	number5["e"] = 5
	fmt.Println("Number4:", number4)

	//key有序输出
	var number6 []string
	for k, _ := range number5 {
		//fmt.Println("k:", k, "-v:", v)    k是无序
		number6 = append(number6, k)
	}
	sort.Strings(number6)

	for _, v := range number6 {
		fmt.Printf("k:%s, v:%d\n", v, number5[v])
	}

	//
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	var number7 map[int][]int
	number7 = make(map[int][]int, 5)

	for i := 0; i < 5; i++ {
		_, ok := number7[i]
		if !ok {
			number7[i] = make([]int, 0, 3)
		}
		number7[i] = append(number7[i], r.Intn(100), r.Intn(100), r.Intn(100))
	}
	fmt.Println(number7)

	//for k, v := range number7 {
	//	_, ok := number7[k]
	//	if !ok {
	//		v = make([]int, 10)
	//	}
	//	v = append(v, r.Intn(100), r.Intn(100), r.Intn(100))
	//	number7[k] = v
	//}
	//
	//fmt.Println(number7)
}

func Demo5() {
	//TryString()
	//TrySlice()
	TryMap()
}

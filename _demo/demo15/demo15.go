package demo15

import "fmt"

type Animal interface {
	Name(name string)
	Eat()
	Talk()
}

type Dog struct {
}

func (d Dog) Name(name string) {
	fmt.Println("Dog Name", name)
}

func (d Dog) Eat() {
	fmt.Println("Dog Eat")
}

func (d Dog) Talk() {
	fmt.Println("Dog Talk")
}

type Cat struct {
}

func (c Cat) Name(name string) {
	fmt.Println("Cat Name", name)
}

func (c Cat) Eat() {
	fmt.Println("Cat Eat")
}

func (c Cat) Talk() {
	fmt.Println("Cat Talk")
}

func Demo15() {
	var d Dog
	var a Animal
	a = d

	a.Name("小白")

	//%T  %V

	//接口 对象的实例  容器
}

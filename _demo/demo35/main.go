package main

import (
	"io/ioutil"
	"fmt"
	"encoding/xml"
)

type Servers struct {
	Name    xml.Name `xml:"servers"`
	Version string   `xml:"version,attr"`
	Servers []Server `xml:"server"`
}

type Server struct {
	ServerName string `xml:"serverName"`
	ServerIP   string `xml:"serverIp"`
}

//xml
// ini 键值对 简单
//https://github.com/go-ini/ini
//https://github.com/go-ini/docs/tree/master/zh-CN
//YAML


//配置文件
func main() {


	data, err := ioutil.ReadFile("./config.xml")
	if err != nil {
		fmt.Printf("Read Failed")
	}

	var servers Servers
	err = xml.Unmarshal(data, &servers)


	//%#v 相应值的Go语法表示
	fmt.Printf("xml: %#v", servers)
}

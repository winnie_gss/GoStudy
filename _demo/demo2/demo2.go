package demo2

import "fmt"

func exam(str string) {
	//type byte = uint8 不带符号8位 (存储2^8=256种情况, 无法存储所有的汉字)
	//type rune = int32 带符号32位

	strlist1 := []byte(str)
	strlist2 := []rune(str)

	fmt.Printf("byte:%s---%b---%d\n", string(strlist1), strlist1, len(strlist1))
	fmt.Printf("rune:%s---%b---%d", string(strlist1), strlist2, len(strlist2))
}

func Demo2() {
	exam("一二三 123 HelloWorld  !")
}

package demo12

import (
	"fmt"
	"encoding/json"
)

//Go的方法是在函数前面加上一个接受者, 这样编译器就知道这个方法属于哪个类型了
//函数不属于任何类型, 方法属于特定的类型

type User struct {
	name    string
	age     int
	content string
}

//可以为当前包定义的类型添加方法
type User2 int

//什么时候用值类型/指针类型作为接受者?
//需要修改接受者中的值; 接受者是大对象的时候,副本拷贝代价比较大; 通常使用指针类型作为接受者

func (u *User) Msg(name string, age int) {
	u.name = name
	u.age = age
}

func (u User) Say() {
	fmt.Printf("name:%s-age:%d\n", u.name, u.age)
}

//匿名函数体与继承

//json在线解析
//json反序列话

type User3 struct {
	Work    string
	Content string
	*People
}

type People struct {
	Name string
	Age  int
}

func Demo12() {

	var user3 = User3{
		Work:    "student",
		Content: "Hello World",
		People: &People{
			Name: "张三",
			Age:  18,
		},
	}

	fmt.Printf("%+v\r", user3)

	//字段名要大写, 不然其它包无法访问
	data, _ := json.Marshal(user3)
	fmt.Println(string(data))

	//反序列化的 对象 与序列化的对象保持相同类型
	var data2 User3
	json.Unmarshal(data, &data2)

	//\r - carriage return, 回车就是输出光标回到本行的开头位置
	//\n - new line, 而换行是移动到下一行
	/*
		*** \r
		_

		*** \n
		***_
		   _

		*** \n\r
		***_
		_
	 */
	//不同运行环境对 CR/LF 的解释表达不完全一致(就是不同操作系统，编辑器对\r \n 处理不同)，

	a := 1
	fmt.Printf("111-%d \r", a)
	fmt.Printf("222-%d \r", a)
	fmt.Printf("333-%d \r", a)

}

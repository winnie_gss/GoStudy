package demo7

//英文官网 : https://golang.org/
//中文官网 : http://zh-golang.appspot.com/
//Golang标准库中文文档 : https://studygolang.com/pkgdoc
//腾讯云开发者手册 : https://cloud.tencent.com/developer/doc/1101

//sort
//flag
import (
	"reflect"
	"fmt"
	_ "gitee.com/winnie_gss/GoStudy/demo7/exam1"
)

func Demo7() {

	var i interface{} = 5
	fmt.Println(reflect.TypeOf(i))

}

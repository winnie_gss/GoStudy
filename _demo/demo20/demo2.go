package demo20

import "fmt"

type Dog2 struct{}

func A1(d Dog2) {
	fmt.Println("A1")
}

func A2(d *Dog2) {
	fmt.Println("A2")
}

func Demo2() {
	//只能调用函数1
	var d1 Dog2
	A1(d1)
	//只能调用函数2
	var d2 *Dog2
	A2(d2)
}

package demo20

import "fmt"

type Animal interface {
	Name(string) string
}

type Dog struct{}

//func (d Dog) Name(name string) string {
//	fmt.Printf("%p\n", &d)
//	return name
//}

func (d *Dog) Name(name string) string {
	fmt.Printf("%p", d)
	return name
}

func Demo1() {
	//方法一和方法二都可以调用(*T是T的超集，*T的方法集包含T )
	var d1 Dog

	//只能调用方法二
	var d2 *Dog

	fmt.Println(d1.Name("d1"))
	fmt.Println(d2.Name("d2"))
}

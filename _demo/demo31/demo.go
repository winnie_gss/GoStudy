package main

import (
	"net"
	"bufio"
	"os"
	"io"
)

func main() {
	conn, _ := net.Dial("tcp", "0.0.0.0:20000")

	/*
	"GET / HTTP/1.1\r\n"
	"HOST"
	"connection:close \r\n"
	"\r\n"
	 */
	io.WriteString(conn, "begin")

	reader := bufio.NewReader(os.Stdin)
	for {
		data, _ := reader.ReadString('\n')
		conn.Write([]byte(data))
	}
}

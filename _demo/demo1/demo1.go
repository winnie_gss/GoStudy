package demo1

import (
	"fmt"
	"runtime"
)

func Demo1() {
	fmt.Println("Hello World!")
}

func SimplePanic(err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Println(file, line, err)
		runtime.Goexit()
	}
}

//https://studygolang.com/
//https://gocn.vip
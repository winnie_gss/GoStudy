package main

//delve 调试工具
//github.com/derekparker/delve/cmd/dlv


//Gostudy debug工具

func exam4(val int) int {
	if val > 0 {
		return val + exam4(val-1)
	} else {
		return val
	}
}

func main() {
	exam4(100)
	exam4(100)
	exam4(100)
}

package main

import (
	"gitee.com/winnie_gss/GoStudy/demo21"
	"time"
)

func initLogger(name, logPath, logName string, level string) {
	m := make(map[string]string, 8)
	m["log_path"] = logPath
	m["log_name"] = logName
	m["log_level"] = level

	err := demo21.InitLogger(name, m)
	if err != nil {
		return
	}

	//demo21.Debug("init logger success")
}

func RunA() {
	i := 0
	for {
		demo21.Debug("user server is runing --A-- %d", i)
		time.Sleep(time.Second)
		i++
	}
}
func RunB() {
	i := 0
	for {
		demo21.Debug("user server is runing --B-- %d", i)
		time.Sleep(time.Second)
		i++
	}
}

func RunC() {
	i := 0
	for {
		demo21.Debug("user server is runing --C-- %d", i)
		time.Sleep(time.Second)
		i++
	}
}

func main() {
	initLogger("file", "C:/logs", "runtime", "debug")

	go RunA()
	go RunB()
	RunC()

	return
}

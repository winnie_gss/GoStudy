package main

import (
	"fmt"
	"sync"
	"time"
)

func SendData(ch chan<- int) {
	ch <- 10
}

func ReadDate(ch <-chan int) {
	fmt.Println(<-ch)
}

func process(i int, wg *sync.WaitGroup) {
	fmt.Println("start Groutine ", i)
	time.Sleep(2 * time.Second)
	fmt.Printf("Goroutine %d ended\n", i)
	wg.Done()   //计数减1
}

func main() {

	no := 3
	var wg sync.WaitGroup  //计数等于0时返回
	for i := 0; i < no; i++ {
		wg.Add(1)  //计数加1
		go process(i, &wg)
	}
	wg.Wait()
	fmt.Println("All go routines finished executing")

	/*
	ch := make(chan int)

	go SendData(ch)
	ReadDate(ch)

	time.Sleep(5 * time.Second)
	*/

	//fmt.Println(runtime.NumCPU())

	//1.6后默认使用全部
	//runtime.GOMAXPROCS()

	//取出一个 len-1
}

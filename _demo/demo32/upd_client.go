package main

import (
	"net"
	"fmt"
)

func main() {
	socket, _ := net.DialUDP("upd4", nil, &net.UDPAddr{
		IP:   net.IPv4(0, 0, 0, 0),
		Port: 8080,
	})

	defer socket.Close()

	senddata := []byte("hello server!")
	_, err := socket.Write(senddata)
	if err != nil{
		
	}


	data := make([]byte, 4096)
	read, remoteAddr, _ := socket.ReadFromUDP(data)
	fmt.Println(read, remoteAddr)
}

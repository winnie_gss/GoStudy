package main

import (
	"net"
	"fmt"
)

func main() {
	listen, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.IPv4(0, 0, 0, 0), Port: 8080})
	if err != nil {

	}

	for {
		var data [1024]byte
		count, addr, err := listen.ReadFromUDP(data[:])
		if err != nil {
			continue
		}

		fmt.Println(string(data[0:count]), addr, count)

		_, err = listen.WriteToUDP([]byte("hello client"), addr)
		if err != nil {
			continue
		}
	}
}

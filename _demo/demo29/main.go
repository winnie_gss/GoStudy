package main

import (
	"fmt"
	"time"
)


/*
func server1(ch chan string) {
	time.Sleep(time.Second * 6)
	ch <- "server1"
}

func server2(ch chan string) {
	time.Sleep(time.Second * 3)
	ch <- "server2"
}
*/

func write(ch chan string) {
	for {
		select {
		case ch <- "hello":
			fmt.Println("ok")
		default:
			fmt.Println("full")
		}
		time.Sleep(time.Second)
	}
}

func main() {

	//  select {}  阻塞

	ch := make(chan string, 5)
	go write(ch)

	for c := range ch {
		fmt.Println(c)
		time.Sleep(time.Second * 10)
	}

	/*
	output1 := make(chan string)
	output2 := make(chan string)

	go server1(output1)
	go server2(output2)

	for {
		select {
		case s1 := <-output1:
			fmt.Println(s1)
		case s2 := <-output2:
			fmt.Println(s2)
		default:  //判断是否满了或为空了

		}
	}
*/

	/*
	s1 := <-output1
	fmt.Println("s1:", s1)

	s2 := <-output2
	fmt.Println("s2:", s2)
	*/

}

package main

import (
	"fmt"
	"sync"
)

//互斥锁

//var x int
//var wg sync.WaitGroup
var mutex sync.Mutex

func Add() {
	for i := 0; i < 5000; i++ {
		mutex.Lock()
		x += 1
		mutex.Unlock()
	}
	wg.Done()
}

func main() {

	wg.Add(2)

	go Add()
	go Add()

	wg.Wait()

	fmt.Println(x)
}

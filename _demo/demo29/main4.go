package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

//原子操作 (大量gorountine 上下文切换频繁)

var x int32
var wg sync.WaitGroup

func Add2() {
	for i := 0; i < 5000; i++ {
		atomic.AddInt32(&x, 1)
	}
	wg.Done()
}

func main() {

	wg.Add(2)

	go Add2()
	go Add2()

	wg.Wait()

	fmt.Println(x)
}

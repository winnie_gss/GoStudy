package main2

import (
	"sync"
	"fmt"
	"time"
)

var x int
var rwlock sync.RWMutex
var wg sync.WaitGroup

//读多写少场景
//当一个gorountine获取写锁之后, 其他的goroutine获取写锁或读锁都会等待
//当一个gorountine获取读锁之后, 其他的goroutine 获取写锁等待 读锁正常(读可以并发)

func writeMsg() {
	rwlock.Lock()
	x = x + 1
	time.Sleep(time.Second * 10)
	rwlock.Unlock()
	wg.Done()
}

func readMsg(i int) {
	rwlock.RLock()
	fmt.Println(i, x)
	time.Sleep(time.Second)
	rwlock.RUnlock()
	wg.Done()
}

func main() {

	wg.Add(1)
	go writeMsg()

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go readMsg(i)
	}

	wg.Wait()
}

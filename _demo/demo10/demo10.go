package demo10

import (
	"strings"
	"fmt"
	"math/rand"
)

//TODO : 写一个程序, 统计一个字符串每个单词出现的次数. 比如: s="how do you do" 输出 how = 1 do = 2 you = 1
func homework1(str string) {
	str2 := strings.Split(str, " ")

	m := make(map[string]int, len(str2))
	for _, v := range str2 {
		_, ok := m[v]
		if ! ok {
			m[v] = 1
		} else {
			m[v] ++
		}
	}

	for k, v := range m {
		fmt.Printf("%s=%d\n", k, v)
	}
}

//TODO : 写一个程序, 实现学生信息的存储, 学生有id, 年龄, 分数等信息. 需要非常方便的通过id查找到对应的学生信息
func homework2() {
	var student map[int]map[string]interface{}
	student = make(map[int]map[string]interface{}, 10)

	key := make([]int, 0, 10)

	for i := 1; i <= 10; i++ {
		value, ok := student[i]
		if !ok {
			value = make(map[string]interface{}, 10)
		}

		value["id"] = i
		value["name"] = fmt.Sprintf("student%d", i)
		value["age"] = rand.Intn(100)
		value["score"] = fmt.Sprintf("%.2f", rand.Float32()*100)

		key = append(key, i)
		student[i] = value
	}

	for _, v := range key {
		fmt.Println(student[v])
	}
}

//TODO : 你有50枚金币, 需要分配给以下几个人: Matthew, Sarah, Augustus, Heidi, Emilie, Peter, Giana, Adriano, Aaron, Elizabeth
//a.名字中包含"a"或"A": 1枚金币
//b.名字中包含"e"或"E": 1枚金币
//c.名字中包含"i"或"l"(不是I): 2枚金币
//d.名字中包含"o"或"O": 3枚金币
//e.名字中包含"u"或"U": 5枚金币
//TODO : 写一个程序, 计算每个用户分到了多少金币, 以及最后剩余多少金币?
func homework3() {
	gold := 50
	m := map[string]int{"Matthew": 0, "Sarah": 0, "Augustus": 0, "Heidi": 0, "Emilie": 0, "Peter": 0, "Giana": 0, "Adriano": 0, "Aaron": 0, "Elizabeth": 0}

	for k, v := range m {

		for _, value := range k {

			switch string(value) {
			case "a", "A", "e", "E":
				v ++
				gold--
			case "i", "l":
				v += 2
				gold -= 2
			case "o", "O":
				v += 3
				gold -= 3
			case "u", "U":
				v += 5
				gold += 5
			}
		}
		m[k] = v
	}

	fmt.Println("每个用户分配到的金币", m)
	fmt.Println("剩余金币", gold)

}

//TODO : 实现一个简单的学生管理系统, 每个学生有分数,年龄,性别, 名字等字段, 用户可以在控制台添加和修改学生信息, 打印所有学生列表的功能

type Student struct {
	Id    int
	Name  string
	Score float32
}

func homework4() {
	/*获取控制台输入的值
	var name string
	fmt.Print("请输入你的名字:")
	fmt.Scan(&name)
	fmt.Println(name)
	*/

	var action int
	var m []map[int]Student   //var m []*Student ; range切片判断是否存在对应的值

	for {
		fmt.Print("请选择你的操作:1-添加学生信息 2-修改学生信息 3-打印所有学生信息 : ")
		_, err := fmt.Scan(&action)
		if err != nil {
			fmt.Println("输入错误!")
			continue
		}

		switch action {
		case 1:
			var id int
			var name string
			var score float32

			fmt.Print("用户Id:")
			fmt.Scan(&id) //fmt.Scanf("%da", &id) 遇到\n时才结束, \n会忽略掉
			fmt.Print("用户Name:")
			fmt.Scan(&name)
			fmt.Print("用户Score:")
			fmt.Scan(&score)

			m1 := make(map[int]Student, 1)
			m1[id] = Student{Id: id, Name: name, Score: score}

			m = append(m, m1)
		case 2:
			var id int
			var name string
			var score float32

			fmt.Print("要修改的用户Id:")
			fmt.Scan(&id)
			fmt.Print("用户Name:")
			fmt.Scan(&name)
			fmt.Print("用户Score:")
			fmt.Scan(&score)

			for k, v := range m {
				for key, _ := range v {
					if key == id {
						m[k][key] = Student{Id: id, Name: name, Score: score}
					}
				}
			}
		case 3:
			fmt.Printf("%v \n", m)
		default:
			fmt.Println("输入错误!")
		}
	}

}

//TODO : 实现一个类似linux的tree命令,能够以树状图的形式列出当前目录下所有文件

func Demo10() {
	//homework1("how do you do")
	//homework2()
	//homework3()
	//homework4()
}

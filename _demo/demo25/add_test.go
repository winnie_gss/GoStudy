package demo25

import (
	"testing"
)

func Add(number1, number2 int) int {
	return number1 + number2
}

func Sub(number1, number2 int) int {
	return number1 - number2
}

//go test -v
//Fatal Error level?

//单元测试的函数名必须以Test开头, 并且只有一个参数 *testing.T
//基准测试或压力测试必须以 Benchmark 开头, 并且只有参数 *testing.B

func TestDemo1(t *testing.T) {
	t.Log("Begin Test")

	n1 := 5
	n2 := 6
	n3 := Add(n1, n2)

	if n3 != 11 {
		t.Error("Error Test")
	}

	t.Log("End Test")
}

//go test -v add_test.go -test.run TestDemo2
func TestDemo2(t *testing.T) {
	t.Log("Begin Test")

	n1 := 5
	n2 := 6
	n3 := Sub(n1, n2)

	if n3 != -1 {
		t.Error("Error Test")
	}

	t.Log("End Test")
}

// go test -beach .
func BenchmarkAdd(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Add(3, 4)
	}
}

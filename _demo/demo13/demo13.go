package demo13

import (
	"flag"
	"fmt"
)

var name string
var age int

func init() {
	//flag包获取命令行参数
	flag.StringVar(&name, "n", "Go", "A Name")
	flag.IntVar(&age, "a", 18, "Age Number")
}

func Demo13() {

	/*
	类型*File
	标准输入 Stdin  = NewFile(uintptr(syscall.Stdin), "/dev/stdin")
	标准输出 Stdout = NemwFile(uintptr(syscall.Stdout), "/dev/stdout")
	标准错误 Stderr = NewFile(uintptr(syscall.Stderr), "/dev/stderr")
	*/

	/*   -----
	number := 1
	fmt.Printf("1--fmt.Printf:%d\n\r", number)
	data1 := fmt.Sprintf("2--fmt.Sprintf:%d", number)
	fmt.Println(data1)

	//var data2 interface{}  interface类型在初始化，是没有分配内存空间给data2变量，相当于nil也就是指一个空指针
	var data2 string
	fmt.Scan(&data2)
	fmt.Println("3--Scan:", data2)
	fmt.Scanf("%s", &data2)
	fmt.Println("4--Scanf:", data2)
	*/

	//终端其实是一个文件
	//以文件方式操作终端

	//不同的运行环境中 console 的 stdin 实现特性不同导致的
	/*
	var b [16]byte
	os.Stdin.Read(b[:])
	fmt.Println(string(b[:]))
	*/

	//从字符串里获取数据 fmt.scanf
	//从文件里获取数据 fmt.Fscanf
	/*
	var a int
	var b string

	fmt.Fscanf(os.Stdin, "%d%s", &a, &b)
	fmt.Println(a, b)
	*/

	//文件本身读写差
	//bufio 带缓冲区文件读写  (大文件)
	/*
	inputReader := bufio.NewReader(os.Stdin)
	input, err := inputReader.ReadString('\n')

	if err == nil{
		fmt.Println(input)
	}
	*/

	//获取命令行参数
	//GoStudy.exe a b c d
	/*
	for _, v := range os.Args {
		fmt.Println(v)
	}
	*/

	//flag包获取命令行参数
	fmt.Println(name)
	fmt.Println(age)

	//github.com/urfave/cli


}

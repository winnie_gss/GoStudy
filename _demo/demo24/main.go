package main

import (
	"reflect"
	"fmt"
)

type MessageBox struct {
	Name    string `tagname:"TagName" json:"JName" aa:"AName"`
	Content string `json:"JContent" bb:"BContent"`
}

func demo1(i interface{}) {
	rType := reflect.TypeOf(i)
	fmt.Println(rType.Field(0).Tag.Get("aa"))
}

func main() {
	MBox := MessageBox{"张三", "HelloWorld"}
	demo1(MBox)
}

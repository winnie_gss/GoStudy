package demo21

import (
	"os"
	"fmt"
)

type ConsoleLogger struct {
	level int
}

func NewConsoleLogger(config map[string]string) (logger LogInterface, err error) {
	level, ok := config["log_level"]
	if !ok {
		err = fmt.Errorf("not fond log_level")
		return
	}

	logger = &ConsoleLogger{level: getLevelNumber(level)}
	return
}

func (f *ConsoleLogger) SetLevel(level int) {
	if level < LogLevelDebug || level > LogLevelFatal {
		level = LogLevelDebug
	}
	f.level = level
}

func (f *ConsoleLogger) Debug(format string, args ...interface{}) {
	if f.level > LogLevelDebug {
		return
	}

	logData := WriteLog(LogLevelDebug, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Trace(format string, args ...interface{}) {
	if f.level > LogLevelTrace {
		return
	}
	logData := WriteLog(LogLevelTrace, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Info(format string, args ...interface{}) {
	if f.level > LogLevelInfo {
		return
	}
	logData := WriteLog(LogLevelInfo, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Warn(format string, args ...interface{}) {
	if f.level > LogLevelWarn {
		return
	}
	logData := WriteLog(LogLevelWarn, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Error(format string, args ...interface{}) {
	if f.level > LogLevelError {
		return
	}
	logData := WriteLog(LogLevelError, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Fatal(format string, args ...interface{}) {
	if f.level > LogLevelFatal {
		return
	}
	logData := WriteLog(LogLevelFatal, format, args...)

	fmt.Fprintf(os.Stdout, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.LineNo, logData.FileName, logData.LevelStr, logData.Message)
}

func (f *ConsoleLogger) Close() {

}

func (f *ConsoleLogger) Init() {

}

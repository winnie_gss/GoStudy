package demo21

import "testing"

func TestFileLogger(t *testing.T) {
	logger := NewFileLogger(LogLevelDebug, "test", "C:/logs")

	logger.Debug("Hello Debug1111")
	logger.Info("Hello Info1111")

	logger.Close()
}

func TestConsoleLogger(t *testing.T) {
	logger := NewConsoleLogger(LogLevelDebug)

	logger.Debug("Hello Console 111")
}

package demo21

import (
	"runtime"
	"time"
	"fmt"
)

type LogData struct {
	Message      string
	TimeStr      string
	LevelStr     string
	FileName     string
	FuncName     string
	LineNo       int
	WarnAndFatal bool
}

func GetLineInfo() (fileName string, funcName string, lineNo int) {
	//skip如果是0，返回当前调用Caller函数的函数名、文件、程序指针PC，1是上一层函数，以此类推(上一个调用的函数)
	pc, file, line, ok := runtime.Caller(4)
	if ok {
		fileName = file
		funcName = runtime.FuncForPC(pc).Name()
		lineNo = line
	}
	return
}

func WriteLog(level int, format string, args ...interface{}) *LogData {
	nowStr := time.Now().Format("2006/1/2 15:04:05")
	levelStr := getLevelText(level)
	fileName, funcName, line := GetLineInfo()
	msg := fmt.Sprintf(format, args...)

	logData := &LogData{
		Message:      msg,
		TimeStr:      nowStr,
		LevelStr:     levelStr,
		FileName:     fileName,
		FuncName:     funcName,
		LineNo:       line,
		WarnAndFatal: false,
	}

	if level == LogLevelWarn || level == LogLevelError || level == LogLevelFatal {
		logData.WarnAndFatal = true
	}

	return logData

	//fmt.Fprintf(file, "[%s] %s:%d  %s \n[%s] %s \n", nowStr, fileName, line, funcName, levelStr, msg)
}

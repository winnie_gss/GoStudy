package demo21

import (
	"os"
	"fmt"
	"strconv"
	"time"
)

type FileLogger struct {
	level         int
	logName       string
	logPath       string
	SuccessFile   *os.File
	ErrorFile     *os.File
	LogDataChan   chan *LogData
	LogSplitType  int
	LogSplitSize  int64
	LastSplitHour int
}

func NewFileLogger(config map[string]string) (logger LogInterface, err error) {
	level, ok := config["log_level"]
	if !ok {
		err = fmt.Errorf("not found log_level")
		return
	}

	logName, ok := config["log_name"]
	if !ok {
		err = fmt.Errorf("not found log_name")
		return
	}

	logPath, ok := config["log_path"]
	if !ok {
		err = fmt.Errorf("not found log_path")
		return
	}

	logChanSize, ok := config["log_chan_size"]
	if !ok {
		logChanSize = "0"
	}

	chanSize, err := strconv.Atoi(logChanSize)
	if err != nil {
		chanSize = 5000
	}

	var logSplitType int = LogSplitTypeHour
	var logSplitSize int64
	logSplitStr, ok := config["log_split_type"]
	if !ok {
		logSplitStr = "hour"
	} else {
		if logSplitStr == "size" {
			logSplitSizeStr, ok := config["log_split_size"]
			if !ok {
				logSplitSizeStr = "104857600"
			}

			logSplitSize, err = strconv.ParseInt(logSplitSizeStr, 10, 64)
			if err != nil {
				logSplitSize = 104857600
			}

			logSplitType = LogSplitTypeSize
		} else {
			logSplitType = LogSplitTypeHour
		}

	}

	logger = &FileLogger{
		level:         getLevelNumber(level),
		logName:       logName,
		logPath:       logPath,
		LogDataChan:   make(chan *LogData, chanSize),
		LogSplitType:  logSplitType,
		LogSplitSize:  logSplitSize,
		LastSplitHour: time.Now().Hour(),
	}
	logger.Init()
	return
}

func (f *FileLogger) Init() {
	filename := fmt.Sprintf("%s/%s_success.log", f.logPath, f.logName)
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0755)
	if err != nil {
		panic(fmt.Sprintf("Open File %s Error: %v", filename, err))
	}
	f.SuccessFile = file

	filename = fmt.Sprintf("%s/%s_error.log", f.logPath, f.logName)
	file, err = os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0755)
	if err != nil {
		panic(fmt.Sprintf("Open File %s Error: %v", filename, err))
	}
	f.ErrorFile = file

	go f.writeLogBackground()
}

func (f *FileLogger) writeLogBackground() {
	for logData := range f.LogDataChan {
		file := f.SuccessFile
		if getLevelNumber(logData.LevelStr) > LogLevelInfo {
			file = f.ErrorFile
		}
		f.checkedSplitFile(logData.WarnAndFatal)

		fmt.Fprintf(file, "[%s] %s:%d  %s \n[%s] %s \n", logData.TimeStr, logData.FileName, logData.FileName, logData.FuncName, logData.LevelStr, logData.Message)
	}
}

//TODO : 时间切割日志 和 大小切割日志
func (f *FileLogger) checkedSplitFile(warnFile bool) {

	//if f.LogSplitType == LogSplitTypeHour {
	//	now := time.Now()
	//	hour := now.Hour()
	//	if hour == f.LastSplitHour {
	//		return
	//	}
	//
	//	f.LastSplitHour = hour
	//	var backupFilename string
	//	var filename string
	//
	//	if warnFile {
	//		backupFilename = fmt.Sprintf("%s/%s_error.log_%04d%02d%02d%02d", f.logPath, f.logName, now.Year(), now.Month(), now.Day(), f.LastSplitHour)
	//		filename = fmt.Sprintf("%s/%s_error.log", f.logPath, f.logName)
	//	} else {
	//		backupFilename = fmt.Sprintf("%s/%s_success.log_%04d%02d%02d%02d", f.logPath, f.logName, now.Year(), now.Month(), now.Day(), f.LastSplitHour)
	//		filename = fmt.Sprintf("%s/%s_success.log", f.logPath, f.logName)
	//	}
	//
	//	file := f.file
	//	if warnFile{
	//
	//	}
	//
	//	file.Close()

	//}

}

func (f *FileLogger) SetLevel(level int) {
	if level < LogLevelDebug || level > LogLevelFatal {
		level = LogLevelDebug
	}
	f.level = level
}

func (f *FileLogger) Debug(format string, args ...interface{}) {
	if f.level > LogLevelDebug {
		return
	}
	logData := WriteLog(LogLevelDebug, format, args...)

	select {
	case f.LogDataChan <- logData:
	default: //队列满了话, 停止写入, 防止阻塞
	}
}

func (f *FileLogger) Trace(format string, args ...interface{}) {
	if f.level > LogLevelTrace {
		return
	}
	logData := WriteLog(LogLevelTrace, format, args...)

	select {
	case f.LogDataChan <- logData:
	default:
	}
}

func (f *FileLogger) Info(format string, args ...interface{}) {
	if f.level > LogLevelInfo {
		return
	}
	logData := WriteLog(LogLevelInfo, format, args...)

	select {
	case f.LogDataChan <- logData:
	default:
	}
}

func (f *FileLogger) Warn(format string, args ...interface{}) {
	if f.level > LogLevelWarn {
		return
	}
	logData := WriteLog(LogLevelWarn, format, args...)

	select {
	case f.LogDataChan <- logData:
	default:
	}
}

func (f *FileLogger) Error(format string, args ...interface{}) {
	if f.level > LogLevelError {
		return
	}
	logData := WriteLog(LogLevelError, format, args...)

	select {
	case f.LogDataChan <- logData:
	default:
	}
}

func (f *FileLogger) Fatal(format string, args ...interface{}) {
	if f.level > LogLevelFatal {
		return
	}
	logData := WriteLog(LogLevelFatal, format, args...)

	select {
	case f.LogDataChan <- logData:
	default:
	}
}

func (f *FileLogger) Close() {
	f.SuccessFile.Close()
	f.ErrorFile.Close()
}

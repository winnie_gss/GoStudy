package main

import (
	"fmt"
	"reflect"
)

//类型信息, 元信息 预先定义好的 reflect.TypeOf
//值类型 程序运行过程中动态改变 reflect.ValueOf

func demo1(i interface{}) {
	re := reflect.TypeOf(i)
	fmt.Println(re)
	fmt.Println(re.Kind())
}

func demo2(i interface{}) {
	re := reflect.ValueOf(i)
	fmt.Println(re)
	fmt.Println(re.Kind())

	fmt.Println(re.Float())
}

//reflect.Ptr
func demo3(i interface{}) {
	re := reflect.ValueOf(i)

	fmt.Println(re.Kind())
	re.Elem().SetFloat(1.34)
}

func demo4(i interface{}) {
	reType := reflect.TypeOf(i)
	reValue := reflect.ValueOf(i)

	kind := reType.Kind()

	switch kind {
	case reflect.Struct:

		for i := 0; i < reValue.NumField(); i++ {
			fmt.Println(reValue.Field(i), reValue.Field(i).Type(), reValue.Field(i).Interface())
		}

	}
}

func demo5(i interface{}) {
	reType := reflect.TypeOf(i)
	reValue := reflect.ValueOf(i)

	kind := reType.Kind()

	switch kind {
	case reflect.Ptr:
		reValue.Elem().Field(0).SetString("李四")
	}
}

type A struct {
	Name string
	Age  int
	Sex  bool
}

func (a A) SetName(name string) {
	a.Name = name
}

func (a A) SetAge(age int) {
	a.Age = age
}

func (a A) GetName() string {
	return a.Name
}

func (a A) GetAge() int {
	return a.Age
}

func (a A) Run1() {
	fmt.Println("A1", a)
}

func (a A) Run2(name string, age int) {
	fmt.Println("A2", name, age)
}

func demo6(i interface{}) {
	rType := reflect.TypeOf(i)
	rValue := reflect.ValueOf(i)

	fmt.Println(rType, rType.NumMethod(), rType.NumField(), rType.Kind())
	fmt.Println(rValue, rValue.Method(0), rValue.Field(0), rValue.Kind())

	fmt.Println(rValue.Field(0).Type(), rValue.Interface(), rValue.FieldByName("Name"))
	fmt.Println(rType.Method(0).Name, rType.Method(3).Type)

	//字母序
	for i := 0; i < rType.NumMethod(); i++ {
		fmt.Println(i, rType.Method(i).Name, rType.Method(i).Type)
	}
}

func demo7(i interface{}) {
	//rType := reflect.TypeOf(i)
	rValue := reflect.ValueOf(i)

	m1 := rValue.MethodByName("Run1")
	var args []reflect.Value
	m1.Call(args)

	m2 := rValue.MethodByName("Run2")
	var args2 []reflect.Value
	argslist := append(args2, reflect.ValueOf("张三"), reflect.ValueOf(18))
	m2.Call(argslist)
}

func main() {

	var a = A{"小明", 25, true}
	//demo6(a)
	demo7(a)

	//var a = A{"小明", 25, true}
	//demo4(a)

	//var a = A{}
	//demo5(&a)
	//fmt.Printf("%v", a)

	/*
	var f float32 = 1.23
	//demo1(f)
	//demo2(f)
	demo3(&f)

	fmt.Println(f)
	*/

}

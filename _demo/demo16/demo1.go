package demo16

import "fmt"

func A(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Println("INT", v)
	case string:
		fmt.Println("String", v)
	default:
		fmt.Println("NOT")
	}
}

func Demo1() {

	i := 1.1
	A(i)

}

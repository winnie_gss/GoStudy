package demo16

import "fmt"

/*
type An struct {
	i interface{}
}
*/

type Animal interface {
	Name()
}

type Dog struct {
}

func (d Dog) Name() {

}

func (a An) Name() {

}

func Demo2() {


	var d Dog
	var a Animal1

	a = d
	//var a Animal

	fmt.Printf("%T", a)

	//值类型实现了一个接口, 指针可以存进去
	//指针类型实现了一个接口, 值类型不能存
	//实现多个接口
	//接口嵌套 结构嵌套
}

package demo14

import (
	"os"
	"fmt"
	"gitee.com/winnie_gss/GoStudy/demo1"
	"io"
	"bufio"
	"io/ioutil"
	"compress/gzip"
)

func Exam1() {
	file, err := os.Open("D:/demo13.go")
	defer file.Close()

	demo1.SimplePanic(err)

	var b [32]byte
	var content []byte

	for {
		n, err := file.Read(b[:])

		//读到没有内容时,报错
		if err == io.EOF {
			break
		}

		demo1.SimplePanic(err)
		//content = append(content, b[:]...)   //最后可能出现没读够
		content = append(content, b[:n]...)
	}

	fmt.Println(string(content))
}

func Exam2() {
	file, err := os.Open("D:/demo13.go")
	demo1.SimplePanic(err)

	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		demo1.SimplePanic(err)

		fmt.Println(line)
	}
}

func Exam3() {
	content, err := ioutil.ReadFile("D:/demo13.go")
	demo1.SimplePanic(err)

	fmt.Println(string(content))
}

func Exam4() {
	file, err := os.Open("D:/demo13.zip")
	demo1.SimplePanic(err)
	defer file.Close()

	reader, err := gzip.NewReader(file)

	demo1.SimplePanic(err)
	fmt.Println(reader)

	//var b [32]byte
	//var content []byte
	//
	//for {
	//	n, err := reader.Read(b[:])
	//	if err == io.EOF {
	//		break
	//	}
	//	demo1.SimplePanic(err)
	//	content = append(content, b[:n]...)
	//}
	//fmt.Println(string(content))
}

func Exam5() {
	//第二个参数: 文件打开模式
	/*
     syscall.O_RDONLY // 只读模式打开文件
     syscall.O_WRONLY // 只写模式打开文件
	 syscall.O_RDWR   // 读写模式打开文件
     syscall.O_APPEND // 写操作时将数据附加到文件尾部
     syscall.O_CREAT  // 如果不存在将创建一个新文件
     syscall.O_EXCL   // 和O_CREATE配合使用，文件必须不存在
     syscall.O_SYNC   // 打开文件用于同步I/O
     syscall.O_TRUNC  // 如果可能，打开时清空文件
	 */
	//第三个参数: 权限控制
	/*
	-rw-r--r--
	-  |  rw-  |  r--  |  r--
	1  |  2    |  3    |  4

	1 文件类型
	2 拥有者权限
	3 群组用户权限
	4 其它用户权限

	r 可读 4
	w 可写 2
	x 可执行 1
	- 无权限 0
	 */
	file, err := os.OpenFile("D:/demo14.go", os.O_TRUNC|os.O_WRONLY, 0666)
	demo1.SimplePanic(err)

	str := "Hello World"
	file.Write([]byte(str))
}

/*
	return x -> 返回值=x , ret指令
 */
func Exam6() (y int) {
	x := 5
	return x
}

func Exam7() {
	file1, err := os.Open("D:/123.txt")
	demo1.SimplePanic(err)

	file2, err := os.OpenFile("D:/321.txt", os.O_CREATE|os.O_WRONLY, 0666)
	demo1.SimplePanic(err)



}

func Demo14() {

	//文件读写
	//Exam1()

	//bufio读取一行数据
	//Exam2()

	//ioutil读取整个文件
	//Exam3()

	//读取压缩文件 ????
	//Exam4()

	//文件写入
	//Exam5()

	//fmt.Println(Exam6())

	//TODO : io.copy 文件拷贝
	Exam7()

}

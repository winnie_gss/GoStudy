package main

import (
	"fmt"
	"math/rand"
	"encoding/json"
	"io/ioutil"
)

type Person struct {
	Name string
	Age  int
}

func writeJson(filename string) (err error) {

	var persons []Person
	for i := 0; i < 10; i++ {
		p := Person{
			Name: fmt.Sprintf("name%d", i),
			Age:  rand.Intn(100),
		}

		persons = append(persons, p)
	}

	data, err := json.Marshal(persons)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = ioutil.WriteFile(filename, data, 0755)
	if err != nil {
		fmt.Println(err)
		return
	}

	return
}

func readJson(filename string) (err error) {
	var person []*Person
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}

	json.Unmarshal(data, &person)

	for _, v := range person{
		fmt.Printf("%v\r\n", v)
	}

	return
}

func main() {
	filename := "D:/person.json"

	//writeJson(filename)

	readJson(filename)
}

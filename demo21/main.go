package main

import (
	"context"
	"time"
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func WorkThing(ctx context.Context) {
	time.Sleep(time.Second)
	//fmt.Println(1)
}

func main() {

	ctx1, cann1 := context.WithCancel(context.Background())
	ctx2, _ := context.WithTimeout(ctx1, time.Second*5)


	go WorkThing(ctx2)


	wg.Add(1)

	for {
		select {
		case <-ctx2.Done():
			wg.Done()
			fmt.Println(1)
			return
		}

	}

	wg.Wait()
	cann1()
}

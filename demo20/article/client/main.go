package main

import (
	"google.golang.org/grpc"
	"fmt"
	pb "gitee.com/winnie_gss/GoStudy/demo20/article/article"
	"time"
	"google.org/x/net/context"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:9090", grpc.WithInsecure())

	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()

	client := pb.NewArticleClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	response, err := client.ArticleList(ctx, &pb.ArticleListRequest{Sort: pb.ARTICLE_SORT_ORDER_BY_TIME})
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(response)
}

package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9001")
	if err != nil {
		fmt.Println(err)
	}

	go func() {
		for{
			var content string
			fmt.Scan(&content)
			conn.Write([]byte(content))
		}
	}()

	go func() {
		for{
			var b [64]byte
			n, err := conn.Read(b[:])
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println(string(b[:n]))
		}
	}()


	time.Sleep(time.Second* 10000)
}

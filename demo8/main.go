package main

import (
	"os"
	"fmt"
	"io"
	"bufio"
	"io/ioutil"
	"time"
)

func ReadFile1(filename string) {

	// os.Create() 和 os.Open() 也是调用了os.OpenFile()

	/*
	*第二个参数: 文件打开模式
	syscall.O_RDONLY // 只读模式打开文件
	syscall.O_WRONLY // 只写模式打开文件
	syscall.O_RDWR   // 读写模式打开文件
	syscall.O_APPEND // 写操作时将数据附加到文件尾部
	syscall.O_CREAT  // 如果不存在将创建一个新文件
	syscall.O_EXCL   // 和O_CREATE配合使用，文件必须不存在
	syscall.O_SYNC   // 打开文件用于同步I/O
	syscall.O_TRUNC  // 如果可能，打开时清空文件
	*/

	/*第三个参数: 权限控制
		-rw-r--r--
		-  |  rw-  |  r--  |  r--
		1  |  2    |  3    |  4

		1 文件类型
		2 拥有者权限
		3 群组用户权限
		4 其它用户权限

		r 可读 4
		w 可写 2
		x 可执行 1
		- 无权限 0
	 */

	//os.O_WRONLY 只写 O_RDWR 读写 (认错单词, 被坑了)
	file, err := os.OpenFile(filename, os.O_RDONLY, 0777)
	if err != nil {
		fmt.Println(err)
	}

	defer file.Close()

	//var b[]byte 长度为0
	var b [64]byte
	var content []byte
	/*
	fmt.Println(reflect.TypeOf(b))
	fmt.Println(reflect.TypeOf(b[:]))
	*/

	for {
		n, err := file.Read(b[:])
		//读到没有内容时,报错
		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println(err)
		}

		//content = append(content, b[:]...)  //最后可能读不够64字节
		content = append(content, b[:n]...)
	}
	fmt.Println(string(content))
}

func ReadFile2(filename string) {
	//带缓冲读取文件
	file, err := os.OpenFile(filename, os.O_RDONLY, 0777)
	if err != nil {
		fmt.Println(err)
	}

	//*file 实现了 Reader接口
	reader := bufio.NewReader(file)

	var b [64]byte
	var content []byte

	for {
		//Read有很多读取数据的方法
		//reader.ReadString('\n') 读取一行数据
		n, err := reader.Read(b[:])

		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println(err)
		}

		content = append(content, b[:n]...)
	}
	fmt.Println(string(content))
}

func ReadFile3(filename string) {
	//一次性读取整个文件
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(content))
}

func WriteFile1(filename string) {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		fmt.Println(err)
	}

	var b []byte
	b = []byte("\nHello World\nHello World2")
	_, err = file.Write(b)
	if err != nil {
		fmt.Println(err)
	}
}

func WriteFile2(filename string) {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		fmt.Println(err)
	}

	var b []byte
	b = []byte("\nHello World\nHello World23")

	write := bufio.NewWriter(file)
	n, err := write.Write(b)

	//Flush方法将缓冲中的数据写入下层的io.Writer接口
	write.Flush()

	fmt.Println(n)
	if err != nil {
		fmt.Println(err)
	}
}

func WriteFile3(filename string) {
	var b []byte
	b = []byte("Hello World\nHello World2")

	//写一个全新的文件
	//os.O_WRONLY|os.O_CREATE|os.O_TRUNC
	err := ioutil.WriteFile(filename, b, 0777)
	if err != nil {
		fmt.Println(err)
	}
}

/*
	类型*File
	标准输入 Stdin  = NewFile(uintptr(syscall.Stdin), "/dev/stdin")
	标准输出 Stdout = NemwFile(uintptr(syscall.Stdout), "/dev/stdout")
	标准错误 Stderr = NewFile(uintptr(syscall.Stderr), "/dev/stderr")
*/
func ReadAndWrite(filename string) {
	//_, err := io.WriteString(os.Stdout, "Hello World")
	//if err != nil {
	//	fmt.Println(err)
	//}

	/*
	Pipe创建一个同步的内存中的管道。它可以用于连接期望io.Reader的代码和期望io.Writer的代码。一端的读取对应另一端的写入，直接在两端拷贝数据，没有内部缓冲。可以安全的并行调用Read和Write或者Read/Write与Close方法。Close方法会在最后一次阻塞中的I/O操作结束后完成。并行调用Read或并行调用Write也是安全的：每一个独立的调用会依次进行。
	 */
	r, w := io.Pipe()

	go func() {
		i := 0
		for {
			w.Write([]byte(fmt.Sprint("Number", i)))
			i++
		}
	}()

	go func() {
		var d [8]byte
		for {
			n, err := r.Read(d[:])
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println(string(d[:n]))
		}
	}()

	time.Sleep(time.Second * 10)
}

func main() {

	var filename = "D:/uploads/file.txt"

	ReadAndWrite(filename)

	//TODO : ReadFile
	//ReadFile1(filename)
	//ReadFile2(filename)
	//ReadFile3(filename)

	//TODO : WriteFile
	//WriteFile1(filename)
	//WriteFile2(filename)
	//WriteFile3(filename)
}

package main

import (
	"fmt"
)

func main() {

	number := make([]func(), 3)

	//闭包可以直接使用外部的变量

	//输出 333
	for i := 0; i < 3; i++ {
		number[i] = func() {
			fmt.Println(i)
		}
	}

	/* 输出 333
	j := 3
	for i := 0; i < 3; i++ {
		number[i] = func() {
			fmt.Println(j)
		}
	}
	*/

	/* 输出 0 1 2
	for i := 0; i < 3; i++ {
		j := i
		number[i] = func() {
			fmt.Println(j)
		}
	}
	*/

	/* 输出0 1 2
	for i := 0; i < 3; i++ {
		number[i] = Number(i)
	}
	*/

	/*
	for i := 0; i < 3; i++ {
		number[i] = func() {
			fmt.Println(i)
		}
		number[i]()
	}
	 */

	for _, v := range number {
		v()
	}
}

func Number(i int) func() {
	return func() {
		fmt.Println(i)
	}
}

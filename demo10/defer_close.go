package main

import (
	"os"
	"io"
)

func CopyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)

	defer src.Close() // 位置1

	if err != nil {
		return
	}

	defer src.Close() //位置2   应该写在位置2，因为如果 Open 失败的话，根本就不用 Close

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}

	written, err = io.Copy(dst, src)
	return
}

package main

import "fmt"

func main() {

	//■■■■■
	s := make([]int, 5, 5)
	fmt.Printf("%p \n", s)

	b := s[2:3]

	//□□■■□
	fmt.Printf("%p \n", s[2:3])

	//□□□■■   b[2:4] 超出索引
	fmt.Printf("%p \n", b[2:3])


}

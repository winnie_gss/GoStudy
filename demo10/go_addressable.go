package main

import (
	"fmt"
)

//go addressable 详解 http://colobu.com/2018/02/27/go-addressable/

func demo1() {
	s := make([]int, 0, 2)
	fmt.Printf("%p, %v \r\n", s, s)

	s = append(s, 0)
	ptr := &s[0]
	*ptr = 9
	fmt.Printf("%p, %v \r\n", s, s)

	s = append(s, 1)
	*ptr = 8
	fmt.Printf("%p, %v \r\n", s, s)

	s = append(s, 2) //append 的时候如果触发扩容，就会复制到新地址去了
	*ptr = 7
	fmt.Printf("%p, %v \r\n", s, s)
}

//Go Hashmap内存布局和实现 :https://ninokop.github.io/2017/10/24/Go-Hashmap%E5%86%85%E5%AD%98%E5%B8%83%E5%B1%80%E5%92%8C%E5%AE%9E%E7%8E%B0/
//Hash，一般翻译做“散列”，也有直接音译为“哈希”的，就是把任意长度的输入（又叫做预映射pre-image）通过散列算法变换成固定长度的输出，该输出就是散列值。这种转换是一种压缩映射，也就是，散列值的空间通常远小于输入的空间，不同的输入可能会散列成相同的输出，所以不可能从散列值来确定唯一的输入值。简单的说就是一种将任意长度的消息压缩到某一固定长度的消息摘要的函数
func demo2() {
	m := make(map[int]string, 2)
	fmt.Printf("%p, %v \r\n", m, m)
	//ptr1 := &m[0]
}

//var number1 []*int  指针切片
//var number2 *[]int  切片指针

func demo3(number1 *[]int) {
	number2 := []int{2, 4}
	*number1 = number2
	fmt.Printf("%p, %v \r\n", *number1, *number1)
}

func demo4(s []int) []int {
	s = append(s, 1, 2, 3)
	return s
}

func main() {
	//TODO : demo3

	var number1 []int
	demo3(&number1)
	fmt.Printf("%p, %v \r\n", number1, number1)

	//TODO : demo4
	/*
	var number []int
	number = demo4(number)  //避免传地址 扩容
	fmt.Printf("%p, %v \r\n", number, number)
	*/
}

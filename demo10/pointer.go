package main

import "fmt"

//定义的时候用* ，取值的时候用&

//& 符号会生成一个指向其作用对象的指针
//* 符号表示指针指向的底层的值

func main() {
	// p是i的地址, *p是i的值, &p 是p的地址   指针变量也是一个变量，这个变量是用来存指针的, 变量本身也有地址
	i := 1
	p := &i

	fmt.Println(*p)
	fmt.Println(p)
	fmt.Println(&p)

}

package main

import (
	"net"
	"fmt"
)

func main() {
	conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 8085})
	if err != nil{
		fmt.Println(err)
	}

	conn.Write([]byte("Hello World"))
}

package main

import (
	"fmt"
	"net"
	"bufio"
	"os"
)

func main() {

	fmt.Print("User Name: ")

	var name string
	_, err := fmt.Scan(&name)
	if err != nil {
		fmt.Println(err)
	}

	conn, err := net.Dial("tcp", "127.0.0.1:8085")
	if err != nil {
		fmt.Println(err)
	}

	defer conn.Close()

	conn.Write([]byte(fmt.Sprint("-----", name)))

	go func() {
		for {
			var b [4096] byte
			n, err := conn.Read(b[:])
			if err != nil {
				continue
			}
			fmt.Println(string(b[:n]))

		}
	}()

	reader := bufio.NewReader(os.Stdin)

	for {
		content, err := reader.ReadString('\n')
		if err != nil {
			continue
		}
		conn.Write([]byte(content))
	}
}

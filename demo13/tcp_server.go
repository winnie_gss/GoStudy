package main

import (
	"net"
	"fmt"
)

func main() {
	listen, err := net.Listen("tcp", "127.0.0.1:8085")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Server Start")

	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		defer conn.Close()

		go func() {
			for {
				var b [4096]byte
				n, err := conn.Read(b[:])
				if err != nil {
					continue
				}

				fmt.Println(string(b[:]))

				_, err = conn.Write(b[:n])
				if err != nil {
					fmt.Println(err)
				}
			}
		}()
	}
}

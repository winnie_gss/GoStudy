package main

import (
	"net"
	"fmt"
)

func main() {
	conn, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.IPv4(0, 0, 0, 0), Port: 8085})

	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		var b [4096]byte
		num, addr, err := conn.ReadFromUDP(b[:])
		if err != nil {
			continue
		}

		fmt.Println(string(b[:num]), addr)

		num, err = conn.WriteToUDP(b[:], addr)
		if err != nil {
			continue
		}
	}

}

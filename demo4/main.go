package main

import (
	"fmt"
	"time"
	"math/rand"
	"sort"
)

func exam1() {
	//map 在使用之前必须用 make 而不是 new 来创建；值为 nil 的 map 是空的，并且不能赋值
	var number1 map[int]string
	number2 := make(map[int]string, 5)
	number3 := map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5}

	fmt.Printf("Number1: %v, len:%d \r\n", number1, len(number1))
	fmt.Printf("Number2: %v, len:%d \r\n", number2, len(number2))
	fmt.Printf("Number3-1: %v, len:%d \r\n", number3, len(number3))

	delete(number3, "c")
	fmt.Printf("Number3-2: %v, len:%d \r\n", number3, len(number3))

	number3["f"] = 6
	fmt.Printf("Number3-3: %v, len:%d \r\n", number3, len(number3))

	//引用类型
	var number4 = map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 4}
	fmt.Println("Number4-1:", number4)
	number5 := number4
	number5["e"] = 5
	fmt.Println("Number4-2:", number4)
}

func exam2() {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var number6 map[int][]int
	number6 = make(map[int][]int, 5)

	for i := 0; i < 5; i++ {
		_, ok := number6[i]
		if !ok {
			number6[i] = make([]int, 0, 3)
		}
		number6[i] = append(number6[i], r.Intn(100), r.Intn(100), r.Intn(100))
	}

	fmt.Println("无序输出")

	for k, v := range number6 {
		fmt.Printf("k: %d, v: %d \r\n", k, v)
	}

	fmt.Println("有序输出")

	var number7 []int
	for k, _ := range number6 {
		number7 = append(number7, k)
	}
	sort.Ints(number7)

	for _, v := range number7 {
		fmt.Printf("k: %d, v: %d \r\n", v, number6[v])
	}
}

//map的key必须是支持==或!=比较运算的类型,不可以是函数,map或slice
//map的value 则没有任何的限制
//make 根据size 大小来初始化分配内存，不过分配后的 map 长度为0。 如果 size 被忽略了，那么会在初始化分配内存的时候 分配一个小尺寸的内存
//map的迭代顺序是随机的
func main() {
	exam2()
}

package main

import (
	"net"
	"fmt"
)

func main() {
	conn, err :=net.Dial("tcp", "127.0.0.1:9090")
	if err != nil{
		fmt.Println(err)
	}

	var b [1024]byte
	n, err :=conn.Read(b[:])
	if err != nil{
		fmt.Println(err)
	}
	fmt.Println(string(b[:n]))
}

/*

upstream goserver{
      server 127.0.0.1:9005;
}

server {
      listen       9090;
      server_name  www.51golang.com ;
      location / {
              proxy_pass http://goserver;
      }
}

 */
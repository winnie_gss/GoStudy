package main

import (
	"fmt"
	"time"
)

//协程不能减少上下文切换次数，但是可以尽量避免线程频繁切换
//调度器 线程 协程 goroutine 基于线程池的M:P:G协程模型

func Worker(ch chan int) {
	for i := 0; i < 10; i++ {
		ch <- i
		fmt.Println("Set i:", i)
	}
}

func exam1() {
	//go 关键字用来创建 goroutine (协程)，是实现并发的关键
	//channel 用来进行多个goroutine通信的，可以设置缓存大小,在未被填满前不会发生阻塞(默认为0,无缓冲)
	//无缓冲的channel是一批数据一进一出, 有缓冲的channel则是一个一个存储，然后一起流出去

	//size = 0  有保证 : 一个无缓冲的channel给你保证被发送的信号已经被接收(因为信号接收发生在信号发送完成之前)
	//ch := make(chan int)

	//size = 1  延迟保证 : 一个 size = 1 的有缓冲 channel 提供延迟保证。它可以保证先前发送的信号已经被接收(因为第一个接收信号，发生在第二个完成的发送信号之前)
	//ch := make(chan int, 1)

	//size > 1 无保证 : 一个 size > 1 的有缓冲的 channel 不会保证发送的信号已经被接收。因为信号发送发生在信号接送完成之前。
	//ch := make(chan int, 2)

	ch := make(chan int, 2)
	go Worker(ch)

	go func() {
		for i := 0; i < 10; i++ {
			<-ch
			fmt.Println("Get i:", i)
			time.Sleep(time.Second * 3)
		}
	}()

	time.Sleep(time.Second * 50)
}

func exam2() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)

	go func() {
		ch1 <- 1
	}()

	go func() {
		ch2 <- 2
	}()

	go func() {
		ch3 <- 3
	}()

	for i := 0; i < 3; i++ {
		select {
		case <-ch1:
			fmt.Println(1)
		case <-ch2:
			fmt.Println(2)
		case <-ch3:
			fmt.Println(3)
		}
	}
}

func main() {
	//exam1()

	exam2()
}

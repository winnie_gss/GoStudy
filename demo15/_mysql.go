package main

import (
	_ "github.com/go-sql-driver/mysql" // 提供驱动
	"database/sql"
	"fmt"
)

var DB *sql.DB

func initDb() error {
	var err error
	dsn := "root:123456@tcp(localhost:3306)/messagebox"
	DB, err = sql.Open("mysql", dsn)

	//最大连接数
	DB.SetMaxOpenConns(10)
	//空闲连接数
	DB.SetMaxIdleConns(2)

	return err
}

type Box struct {
	Id      int            `db:"id"`
	Title   sql.NullString `db:"title"`
	Content string         `db:"content"`
}

func selectOne() {
	sqlstr := "select id, title, content from msg_article where id=?"
	row := DB.QueryRow(sqlstr, 2)

	var box Box

	err := row.Scan(&box.Id, &box.Title, &box.Content)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("id:%d, title:%v, content:%s", box.Id, box.Title, box.Content)
}

func selectOne2() {
	sqlstr := "select id, title, content from msg_article where id=?"
	row := DB.QueryRow(sqlstr, 2)

	if row != nil {
		return
	}

	var box Box
	err := row.Scan(&box.Id, &box.Title, &box.Content)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("id:%d, title:%v, content:%s", box.Id, box.Title, box.Content)
}

func selectAll() {
	sqlstr := "select id, title, content from msg_article limit 10"
	row, err := DB.Query(sqlstr)

	if err != nil {
		fmt.Println(err)
		return
	}

	//结果集内数据没读出来,连接就不会释放
	defer func() {
		if row != nil {
			row.Close()
		}
	}()

	for row.Next() {
		var box Box

		err := row.Scan(&box.Id, &box.Title, &box.Content)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("id:%d, title:%v, content:%s \n", box.Id, box.Title, box.Content)
	}

}

func updateSql() {
	sqlstr := "insert into msg_article(title, content) values(?, ?)"

	result, err := DB.Exec(sqlstr, "标题1", "内容1")
	if err != nil {
		fmt.Println(err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(id)
}

func updateSql2() {
	sqlstr := "insert into msg_article(title, content) values(?, ?)"
	//预处理可以缓存sql语句
	//使用预处理的话, 内容和数据是分两部分传的
	stmt, err := DB.Prepare(sqlstr)
	if err != nil {
		fmt.Println(err)
	}

	//defer stmt close

	result, err := stmt.Exec("标题1", "内容1")
	if err != nil {
		fmt.Println(err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(id)
}

//原子性（Atomicity，或称不可分割性）、一致性（Consistency）、隔离性（Isolation，又称独立性）、持久性（Durability）
func transaction() {
	conn, _ := DB.Begin() //开启一个事务

	conn.Rollback() //回滚一个事务
	conn.Commit() //提交一个事务
}

func main() {
	err := initDb()
	if err != nil {
		fmt.Println("init db failed", err)
		return
	}

	//selectOne()

	/*
	for i:=0; i<20;i++  {
		fmt.Print(i, "------")
		selectOne2()
		fmt.Println()
	}
	*/

	//selectAll()

	//updateSql()

	//updateSql2()

	transaction()
}

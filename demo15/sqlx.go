package main

import (
	_ "github.com/go-sql-driver/mysql" // 提供驱动
	"fmt"
	"github.com/jmoiron/sqlx" //在原生sql上进行封装
	"database/sql"
)

var DB *sqlx.DB

func initDb() error {
	var err error
	dsn := "root:123456@tcp(localhost:3306)/messagebox"
	DB, err = sqlx.Open("mysql", dsn)

	//最大连接数
	DB.SetMaxOpenConns(10)
	//空闲连接数
	DB.SetMaxIdleConns(2)

	return err
}

type Box struct {
	Id      int            `db:"id"`
	Title   sql.NullString `db:"title"` //数据库字段选上非null, 可以避免
	Content string         `db:"content"`
}

func selectOne() {
	sqlstr := "select id, title, content from msg_article where id=?"

	var box Box
	err := DB.Get(&box, sqlstr, 2)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("%v", box)
}

func selectAll() {
	sqlstr := "select id, title, content from msg_article limit 10"

	var box []Box
	err := DB.Select(&box, sqlstr)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(box)
}

func main() {
	err := initDb()
	if err != nil {
		fmt.Println("init db failed", err)
		return
	}

	//selectOne()

	selectAll()


}

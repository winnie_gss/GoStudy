package main

//强类型化的语言，具有垃圾回收机制， 并显式支持并发编程

/*
uint8       the set of all unsigned  8-bit integers (0 to 255)
uint16      the set of all unsigned 16-bit integers (0 to 65535)

int8        the set of all signed  8-bit integers (-128 to 127)
int16       the set of all signed 16-bit integers (-32768 to 32767)
 */

type B struct {

}

type A struct {
	a B
	b *B
}


func main() {

}

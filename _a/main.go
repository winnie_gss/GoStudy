package main

import "fmt"

func main() {

	//致命错误, 只退出当前协程
	var i interface{} = 1
	fmt.Println(i.(string))

}

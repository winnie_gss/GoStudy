package main

import (
	"net"
	"fmt"
	"net/http"
)

func main() {






	var connList []net.Conn

	listen, err := net.Listen("tcp", ":9001")
	if err != nil {
		fmt.Println(err)
	}

	for {
		conn, err := listen.Accept() //阻塞直到有客户端连接
		connList = append(connList, conn)
		if err != nil {
			fmt.Println(err)
		}

		var ch = make(chan []byte, 1000)

		go func(ch chan []byte) {
			for {
				var b [64]byte
				n, err := conn.Read(b[:])
				if err != nil {
					fmt.Println(err)
				}

				if n > 0 {
					ch <- b[:]
				}
			}
		}(ch)

		go func(ch chan []byte) {
			for {
				select {
				case data := <-ch:
					for _, v := range connList {
						v.Write(data)
					}
				}
			}
		}(ch)
	}

}

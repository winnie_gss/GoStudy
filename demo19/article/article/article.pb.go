// Code generated by protoc-gen-go. DO NOT EDIT.
// source: article.proto

package article

/*
留言板
*/

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type RRC_RESULT int32

const (
	RRC_RESULT_RESULT_FAIL    RRC_RESULT = 0
	RRC_RESULT_RESULT_SUCCESS RRC_RESULT = 1
)

var RRC_RESULT_name = map[int32]string{
	0: "RESULT_FAIL",
	1: "RESULT_SUCCESS",
}
var RRC_RESULT_value = map[string]int32{
	"RESULT_FAIL":    0,
	"RESULT_SUCCESS": 1,
}

func (x RRC_RESULT) String() string {
	return proto.EnumName(RRC_RESULT_name, int32(x))
}
func (RRC_RESULT) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_article_ec80b8871a0fabee, []int{0}
}

type ARTICLE_SORT int32

const (
	ARTICLE_SORT_ORDER_BY_TIME  ARTICLE_SORT = 0
	ARTICLE_SORT_ORDER_BY_CLICK ARTICLE_SORT = 1
)

var ARTICLE_SORT_name = map[int32]string{
	0: "ORDER_BY_TIME",
	1: "ORDER_BY_CLICK",
}
var ARTICLE_SORT_value = map[string]int32{
	"ORDER_BY_TIME":  0,
	"ORDER_BY_CLICK": 1,
}

func (x ARTICLE_SORT) String() string {
	return proto.EnumName(ARTICLE_SORT_name, int32(x))
}
func (ARTICLE_SORT) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_article_ec80b8871a0fabee, []int{1}
}

type ArticleStruct struct {
	Id                   uint32   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Title                string   `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Content              []byte   `protobuf:"bytes,3,opt,name=content,proto3" json:"content,omitempty"`
	Viewnum              uint32   `protobuf:"varint,4,opt,name=viewnum,proto3" json:"viewnum,omitempty"`
	Time                 uint32   `protobuf:"varint,5,opt,name=time,proto3" json:"time,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ArticleStruct) Reset()         { *m = ArticleStruct{} }
func (m *ArticleStruct) String() string { return proto.CompactTextString(m) }
func (*ArticleStruct) ProtoMessage()    {}
func (*ArticleStruct) Descriptor() ([]byte, []int) {
	return fileDescriptor_article_ec80b8871a0fabee, []int{0}
}
func (m *ArticleStruct) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ArticleStruct.Unmarshal(m, b)
}
func (m *ArticleStruct) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ArticleStruct.Marshal(b, m, deterministic)
}
func (dst *ArticleStruct) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ArticleStruct.Merge(dst, src)
}
func (m *ArticleStruct) XXX_Size() int {
	return xxx_messageInfo_ArticleStruct.Size(m)
}
func (m *ArticleStruct) XXX_DiscardUnknown() {
	xxx_messageInfo_ArticleStruct.DiscardUnknown(m)
}

var xxx_messageInfo_ArticleStruct proto.InternalMessageInfo

func (m *ArticleStruct) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ArticleStruct) GetTitle() string {
	if m != nil {
		return m.Title
	}
	return ""
}

func (m *ArticleStruct) GetContent() []byte {
	if m != nil {
		return m.Content
	}
	return nil
}

func (m *ArticleStruct) GetViewnum() uint32 {
	if m != nil {
		return m.Viewnum
	}
	return 0
}

func (m *ArticleStruct) GetTime() uint32 {
	if m != nil {
		return m.Time
	}
	return 0
}

type ArticleListRequest struct {
	Sort                 ARTICLE_SORT `protobuf:"varint,1,opt,name=sort,proto3,enum=article.ARTICLE_SORT" json:"sort,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *ArticleListRequest) Reset()         { *m = ArticleListRequest{} }
func (m *ArticleListRequest) String() string { return proto.CompactTextString(m) }
func (*ArticleListRequest) ProtoMessage()    {}
func (*ArticleListRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_article_ec80b8871a0fabee, []int{1}
}
func (m *ArticleListRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ArticleListRequest.Unmarshal(m, b)
}
func (m *ArticleListRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ArticleListRequest.Marshal(b, m, deterministic)
}
func (dst *ArticleListRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ArticleListRequest.Merge(dst, src)
}
func (m *ArticleListRequest) XXX_Size() int {
	return xxx_messageInfo_ArticleListRequest.Size(m)
}
func (m *ArticleListRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ArticleListRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ArticleListRequest proto.InternalMessageInfo

func (m *ArticleListRequest) GetSort() ARTICLE_SORT {
	if m != nil {
		return m.Sort
	}
	return ARTICLE_SORT_ORDER_BY_TIME
}

type ArticleListResponse struct {
	Code                 int32            `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Msg                  string           `protobuf:"bytes,2,opt,name=msg,proto3" json:"msg,omitempty"`
	List                 []*ArticleStruct `protobuf:"bytes,3,rep,name=list,proto3" json:"list,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *ArticleListResponse) Reset()         { *m = ArticleListResponse{} }
func (m *ArticleListResponse) String() string { return proto.CompactTextString(m) }
func (*ArticleListResponse) ProtoMessage()    {}
func (*ArticleListResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_article_ec80b8871a0fabee, []int{2}
}
func (m *ArticleListResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ArticleListResponse.Unmarshal(m, b)
}
func (m *ArticleListResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ArticleListResponse.Marshal(b, m, deterministic)
}
func (dst *ArticleListResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ArticleListResponse.Merge(dst, src)
}
func (m *ArticleListResponse) XXX_Size() int {
	return xxx_messageInfo_ArticleListResponse.Size(m)
}
func (m *ArticleListResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ArticleListResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ArticleListResponse proto.InternalMessageInfo

func (m *ArticleListResponse) GetCode() int32 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *ArticleListResponse) GetMsg() string {
	if m != nil {
		return m.Msg
	}
	return ""
}

func (m *ArticleListResponse) GetList() []*ArticleStruct {
	if m != nil {
		return m.List
	}
	return nil
}

func init() {
	proto.RegisterType((*ArticleStruct)(nil), "article.ArticleStruct")
	proto.RegisterType((*ArticleListRequest)(nil), "article.ArticleListRequest")
	proto.RegisterType((*ArticleListResponse)(nil), "article.ArticleListResponse")
	proto.RegisterEnum("article.RRC_RESULT", RRC_RESULT_name, RRC_RESULT_value)
	proto.RegisterEnum("article.ARTICLE_SORT", ARTICLE_SORT_name, ARTICLE_SORT_value)
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ArticleClient is the client API for Article service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ArticleClient interface {
	// TODO : 留言板列表页
	ArticleList(ctx context.Context, in *ArticleListRequest, opts ...grpc.CallOption) (*ArticleListResponse, error)
}

type articleClient struct {
	cc *grpc.ClientConn
}

func NewArticleClient(cc *grpc.ClientConn) ArticleClient {
	return &articleClient{cc}
}

func (c *articleClient) ArticleList(ctx context.Context, in *ArticleListRequest, opts ...grpc.CallOption) (*ArticleListResponse, error) {
	out := new(ArticleListResponse)
	err := c.cc.Invoke(ctx, "/article.Article/ArticleList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ArticleServer is the server API for Article service.
type ArticleServer interface {
	// TODO : 留言板列表页
	ArticleList(context.Context, *ArticleListRequest) (*ArticleListResponse, error)
}

func RegisterArticleServer(s *grpc.Server, srv ArticleServer) {
	s.RegisterService(&_Article_serviceDesc, srv)
}

func _Article_ArticleList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ArticleListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ArticleServer).ArticleList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/article.Article/ArticleList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ArticleServer).ArticleList(ctx, req.(*ArticleListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Article_serviceDesc = grpc.ServiceDesc{
	ServiceName: "article.Article",
	HandlerType: (*ArticleServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ArticleList",
			Handler:    _Article_ArticleList_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "article.proto",
}

func init() { proto.RegisterFile("article.proto", fileDescriptor_article_ec80b8871a0fabee) }

var fileDescriptor_article_ec80b8871a0fabee = []byte{
	// 363 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x52, 0x4d, 0x6f, 0x9b, 0x40,
	0x14, 0x34, 0x36, 0xae, 0xd5, 0x67, 0xe3, 0xd2, 0xd7, 0x0f, 0xa1, 0xb6, 0x07, 0x0b, 0xf5, 0x40,
	0x2d, 0x15, 0xa9, 0xae, 0x7a, 0xae, 0x6c, 0x42, 0x24, 0x12, 0x22, 0x4b, 0x0b, 0x3e, 0xe4, 0x84,
	0x6c, 0xbc, 0xb2, 0x56, 0x32, 0xac, 0xc3, 0x2e, 0x49, 0x2e, 0xf9, 0xef, 0x11, 0x0b, 0x24, 0x24,
	0xca, 0x6d, 0xe6, 0xed, 0xfb, 0x98, 0x19, 0x2d, 0x18, 0xdb, 0x42, 0xb2, 0xf4, 0x48, 0xdd, 0x53,
	0xc1, 0x25, 0xc7, 0x51, 0x43, 0xed, 0x07, 0x30, 0x96, 0x35, 0x8c, 0x64, 0x51, 0xa6, 0x12, 0xa7,
	0xd0, 0x67, 0x7b, 0x4b, 0x9b, 0x69, 0x8e, 0x41, 0xfa, 0x6c, 0x8f, 0x9f, 0x61, 0x28, 0x99, 0x3c,
	0x52, 0xab, 0x3f, 0xd3, 0x9c, 0xf7, 0xa4, 0x26, 0x68, 0xc1, 0x28, 0xe5, 0xb9, 0xa4, 0xb9, 0xb4,
	0x06, 0x33, 0xcd, 0x99, 0x90, 0x96, 0x56, 0x2f, 0xb7, 0x8c, 0xde, 0xe5, 0x65, 0x66, 0xe9, 0x6a,
	0x49, 0x4b, 0x11, 0x41, 0x97, 0x2c, 0xa3, 0xd6, 0x50, 0x95, 0x15, 0xb6, 0xff, 0x03, 0x36, 0xe7,
	0x43, 0x26, 0x24, 0xa1, 0x37, 0x25, 0x15, 0x12, 0x7f, 0x81, 0x2e, 0x78, 0x21, 0x95, 0x8a, 0xe9,
	0xe2, 0x8b, 0xdb, 0x6a, 0x5f, 0x92, 0x38, 0xf0, 0x42, 0x3f, 0x89, 0xd6, 0x24, 0x26, 0xaa, 0xc5,
	0x3e, 0xc0, 0xa7, 0x17, 0x0b, 0xc4, 0x89, 0xe7, 0x82, 0x56, 0xb7, 0x52, 0xbe, 0xa7, 0x6a, 0xc3,
	0x90, 0x28, 0x8c, 0x26, 0x0c, 0x32, 0x71, 0x68, 0x7c, 0x54, 0x10, 0xe7, 0xa0, 0x1f, 0x99, 0xa8,
	0x2c, 0x0c, 0x9c, 0xf1, 0xe2, 0xeb, 0xf3, 0x9d, 0x6e, 0x22, 0x44, 0xf5, 0xcc, 0xff, 0x00, 0x10,
	0xe2, 0x25, 0xc4, 0x8f, 0x36, 0x61, 0x8c, 0x1f, 0x60, 0x5c, 0xa3, 0xe4, 0x7c, 0x19, 0x84, 0x66,
	0x0f, 0x11, 0xa6, 0x4d, 0x21, 0xda, 0x78, 0x9e, 0x1f, 0x45, 0xa6, 0x36, 0xff, 0x07, 0x93, 0xae,
	0x62, 0xfc, 0x08, 0xc6, 0x9a, 0x9c, 0xf9, 0x24, 0x59, 0x5d, 0x27, 0x71, 0x70, 0xe5, 0xd7, 0x63,
	0x4f, 0x25, 0x2f, 0x0c, 0xbc, 0x4b, 0x53, 0x5b, 0x6c, 0x60, 0xd4, 0x08, 0xc0, 0x0b, 0x18, 0x77,
	0xdc, 0xe1, 0xf7, 0xd7, 0x0a, 0x3b, 0xa1, 0x7d, 0xfb, 0xf1, 0xf6, 0x63, 0x1d, 0x88, 0xdd, 0x5b,
	0xfd, 0x04, 0x5b, 0xf0, 0xb2, 0x48, 0xe9, 0x6f, 0x7e, 0xa2, 0xb9, 0x9b, 0xf2, 0xcc, 0xcd, 0xa8,
	0x10, 0xdb, 0x03, 0xdd, 0xf1, 0xfb, 0x76, 0x76, 0xf7, 0x4e, 0xfd, 0x8f, 0xbf, 0x8f, 0x01, 0x00,
	0x00, 0xff, 0xff, 0x7f, 0x74, 0xad, 0xd5, 0x30, 0x02, 0x00, 0x00,
}

package main

import (
	"log"
	"net"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pb "gitee.com/winnie_gss/GoStudy/demo19/article/article"
	"golang.org/x/net/context"
)

type server struct {
}

func (s *server) ArticleList(ctx context.Context, request *pb.ArticleListRequest) (response *pb.ArticleListResponse, err error) {
	var list []*pb.ArticleStruct
	return &pb.ArticleListResponse{Code: int32(pb.RRC_RESULT_RESULT_SUCCESS), Msg: "", List: list}, nil
}

func main() {
	var ss = &server{}

	listen, err := net.Listen("tcp", "127.0.0.1:7002")
	if err != nil {
		fmt.Println(err)
	}
	s := grpc.NewServer()
	pb.RegisterArticleServer(s, ss)
	reflection.Register(s)
	if err := s.Serve(listen); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
